const PORT = 3000;
const MONGO_DB_NAME = "zellliew-node-crud";

const EXPRESS = require("express");
const BODY_PARSER = require("body-parser");
const MONGO_CLIENT = require("mongodb").MongoClient;
const APP = EXPRESS();

APP.use(BODY_PARSER.urlencoded({extended: true}));
APP.set("view engine", "ejs");

var appDatabase;
MONGO_CLIENT.connect("mongodb://root:root@ds161210.mlab.com:61210/" + MONGO_DB_NAME,
    (error, database) => {
        if(error) return console.error(error);

        appDatabase = database;
        APP.listen(PORT,
            () => {
                console.info("Listening on port " + PORT);
            }
        );
    }
)

APP.get("/",
    (request, response) => {
        appDatabase.collection(MONGO_DB_NAME)
            .find()
            .toArray((error, result) => {
                if(error) return console.error(error);

                response.render("index.ejs", { quotes: result });
            }
        );
    }
)

APP.post("/quotes",
    (request, response) => {
        appDatabase.collection(MONGO_DB_NAME)
            .save(request.body,
                (error, result) => {
                    if(error) return console.error(error);

                    console.log("Saved to database: " + request.body);
                    response.redirect("/");
                }
            )
    }
)